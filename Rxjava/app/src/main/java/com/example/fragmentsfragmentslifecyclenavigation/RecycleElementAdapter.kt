package com.example.fragmentsfragmentslifecyclenavigation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.fragmentsfragmentslifecyclenavigation.databinding.RecycleElementBinding

class RecycleElementAdapter: RecyclerView.Adapter<RecycleElementAdapter.ElementHolder>() {
    val strokaList = ArrayList<RecycleElement>()

    class ElementHolder(item: View): RecyclerView.ViewHolder(item) {
        val binding = RecycleElementBinding.bind(item)
        fun bind(element: RecycleElement) = with(binding){
            elementText.text = element.stroka
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ElementHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycle_element, parent, false)
        return ElementHolder(view)
    }

    override fun onBindViewHolder(holder: ElementHolder, position: Int) {
        holder.bind(strokaList[position])
    }

    override fun getItemCount(): Int {
        return strokaList.size
    }

    fun addstroki(list: List<RecycleElement>) {
        strokaList.clear()
        strokaList.addAll(list)
        notifyDataSetChanged()
    }
}