package com.example.fragmentsfragmentslifecyclenavigation.ui.secondfragemnt

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SecondViewModel : ViewModel() {
    private val _email = MutableLiveData<String>().apply {
        value = ""
    }
    val email: LiveData<String> = _email

    private val _password = MutableLiveData<String>().apply {
        value = ""
    }
    val password: LiveData<String> = _password
}