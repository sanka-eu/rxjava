package com.example.fragmentsfragmentslifecyclenavigation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.fragmentsfragmentslifecyclenavigation.MyDataItem
import com.example.fragmentsfragmentslifecyclenavigation.R
import com.example.fragmentsfragmentslifecyclenavigation.databinding.RecycleElementBinding

class DataAdapter: RecyclerView.Adapter<DataAdapter.DataHolder>() {

    val elementList = ArrayList<MyDataItem>()
    var onItemClick: ((MyDataItem) -> Unit)? = null

    class DataHolder(item: View): RecyclerView.ViewHolder(item) {

        val binding = RecycleElementBinding.bind(item)

        fun bind(data: MyDataItem) = with(binding) {
            elementText.text =  data.id.toString() + " " + data.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycle_element, parent, false)
        return DataHolder(view)
    }

    override fun onBindViewHolder(holder: DataHolder, position: Int) {
        holder.bind(elementList[position])
    }

    override fun getItemCount(): Int {
        return elementList.size
    }

    fun addstroki(list: List<MyDataItem>) {
        elementList.clear()
        elementList.addAll(list)
        notifyDataSetChanged()
    }
}