package com.example.fragmentsfragmentslifecyclenavigation.ui.secondfragemnt

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.fragmentsfragmentslifecyclenavigation.ApiInterface
import com.example.fragmentsfragmentslifecyclenavigation.DataAdapter
import com.example.fragmentsfragmentslifecyclenavigation.MyDataItem
import com.example.fragmentsfragmentslifecyclenavigation.R
import com.example.fragmentsfragmentslifecyclenavigation.databinding.FragmentSecondBinding
import com.example.fragmentsfragmentslifecyclenavigation.ui.db.MyDBManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_second.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlinx.android.synthetic.main.*
import kotlinx.android.synthetic.main.recycle_element.*

const val BASE_URL = "https://jsonplaceholder.typicode.com/"

class SecondFragment : Fragment() {
    private lateinit var viewModel: SecondViewModel
    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!
    private val adapter = DataAdapter()

    companion object {
        fun newInstance() = SecondFragment()
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this)[SecondViewModel::class.java]
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val toFirstFragment = binding.btnToFirstFragment
        val uploadData  = binding.btnUploadData
        val uploadJson = binding.btnUploadJson

        uploadData.setOnClickListener() {
            getMyData()
            textViewSecond.text = "Loading successful!"
        }

        uploadJson.setOnClickListener() {
            //textViewSecond.text
        }

        toFirstFragment.setOnClickListener {
            findNavController().navigate(R.id.action_registration_to_login)
        }

        return root
    }

    private fun getMyData() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiInterface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object : Callback<List<MyDataItem>?> {
            override fun onResponse(
                call: Call<List<MyDataItem>?>,
                response: Response<List<MyDataItem>?>
            ) {
                val responseBody = response.body()!!
                var spisok = ArrayList<MyDataItem>()

                val myStringBuilder = StringBuilder()
                responseBody.forEach { myData ->
                    myStringBuilder.append(myData.id)
                    myStringBuilder.append(" ")
                    myStringBuilder.append(myData.title)
                    spisok.add(myData)
                }
                init(spisok)
            }

            override fun onFailure(call: Call<List<MyDataItem>?>, t: Throwable) {
                Log.d("MainActivity", "onFailure"+t.message)
            }
        })
    }

    private fun init(spisok: ArrayList<MyDataItem>) {
        binding.apply {
            adapter.addstroki(spisok)
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}