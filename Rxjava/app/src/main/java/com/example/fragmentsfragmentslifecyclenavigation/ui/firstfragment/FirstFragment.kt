package com.example.fragmentsfragmentslifecyclenavigation.ui.firstfragment

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.fragmentsfragmentslifecyclenavigation.ApiInterface
import com.example.fragmentsfragmentslifecyclenavigation.DataAdapter
import com.example.fragmentsfragmentslifecyclenavigation.MyDataItem
import com.example.fragmentsfragmentslifecyclenavigation.R
import com.example.fragmentsfragmentslifecyclenavigation.databinding.FragmentFirstBinding
import com.example.fragmentsfragmentslifecyclenavigation.ui.db.MyDBManager
import com.example.fragmentsfragmentslifecyclenavigation.ui.secondfragemnt.BASE_URL
import com.google.android.material.bottomnavigation.BottomNavigationView
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_first.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class FirstFragment : Fragment() {
    private lateinit var viewModel: FirstViewModel
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!
    private val adapter = DataAdapter()

    companion object {
        fun newInstance() = FirstFragment()
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this)[FirstViewModel::class.java]
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val toSecondFragment = binding.btnToSecondFragment
        val start  = binding.btnStart

        start.setOnClickListener() {

            val dispose = dataSource()
                .subscribeOn(Schedulers.newThread())
                .subscribe({
                textViewFirst.text = "Loading successful"
            }, {
                Log.d("MYTAG", "Error observable")
                textViewFirst.text = "Error!"
            })
        }

        toSecondFragment.setOnClickListener {
            findNavController().navigate(R.id.action_login_to_registration)
        }

        return root
    }

    private fun dataSource(): Observable<Int> {
        return Observable.create { subscriber ->
            for (i in 1..200) {
                Thread.sleep(500)
                getMyData()
                subscriber.onNext(i)
            }
        }
    }

    private fun getMyData() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiInterface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object : Callback<List<MyDataItem>?> {
            override fun onResponse(
                call: Call<List<MyDataItem>?>,
                response: Response<List<MyDataItem>?>
            ) {
                val responseBody = response.body()!!
                var spisok = ArrayList<MyDataItem>()

                val myStringBuilder = StringBuilder()
                responseBody.forEach { myData ->
                    myStringBuilder.append(myData.id)
                    myStringBuilder.append(" ")
                    myStringBuilder.append(myData.title)
                    spisok.add(myData)
                }
                init(spisok)
            }

            override fun onFailure(call: Call<List<MyDataItem>?>, t: Throwable) {
                Log.d("MainActivity", "onFailure"+t.message)
            }
        })
    }

    private fun init(spisok: ArrayList<MyDataItem>) {
        binding.apply {
            adapter.addstroki(spisok)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}