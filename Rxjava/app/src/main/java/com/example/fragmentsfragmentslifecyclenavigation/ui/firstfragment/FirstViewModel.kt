package com.example.fragmentsfragmentslifecyclenavigation.ui.firstfragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class FirstViewModel : ViewModel() {
    private val _email = MutableLiveData<String>().apply {
        value = ""
    }
    val email: LiveData<String> = _email

    private val _password = MutableLiveData<String>().apply {
        value = ""
    }
    val password: LiveData<String> = _password
}