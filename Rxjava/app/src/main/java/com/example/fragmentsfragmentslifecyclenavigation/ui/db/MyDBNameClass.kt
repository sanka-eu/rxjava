package com.example.fragmentsfragmentslifecyclenavigation.ui.db

import android.provider.BaseColumns

object MyDBNameClass: BaseColumns {
    const val TABLE_NAME = "Users"
    const val COLUMN_NAME = "Name"
    const val COLUMN_SURNAME = "Surname"
    const val COLUMN_EMAIL = "Email"
    const val COLUMN_PASSWORD = "Password"

    const val DATABASE_VERSION = 1
    const val DATABASE_NAME = "TEST_DB.db"

    const val CREATE_TABLE = "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
            "$COLUMN_NAME TEXT, " +
            "$COLUMN_SURNAME TEXT, $COLUMN_EMAIL TEXT PRIMARY KEY, $COLUMN_PASSWORD TEXT"
    const val SQL_DELETE_TABLE = "DROP TABLE IF EXISTS $TABLE_NAME"
}