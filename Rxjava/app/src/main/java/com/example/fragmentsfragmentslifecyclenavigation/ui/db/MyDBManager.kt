package com.example.fragmentsfragmentslifecyclenavigation.ui.db

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.view.LayoutInflater

class MyDBManager(val context: Context) {
    private val myDBHelper = DBHelper(context)
    var db: SQLiteDatabase? = null
    fun openDB() {
        db = myDBHelper.writableDatabase
    }
    suspend fun addUser(Name: String, Surname: String, Email: String, Password: String){
        val values = ContentValues().apply {
            put(MyDBNameClass.COLUMN_NAME, Name)
            put(MyDBNameClass.COLUMN_SURNAME, Surname)
            put(MyDBNameClass.COLUMN_EMAIL, Email)
            put(MyDBNameClass.COLUMN_PASSWORD, Password)
        }
        db?.insert(MyDBNameClass.TABLE_NAME, null, values)
    }

    @SuppressLint("Range")
    suspend fun readDbDataLog(Email: String, Password: String) : Boolean {
        var dataList = false
        val cursor = db?.query(MyDBNameClass.TABLE_NAME, null, null, null,
            null, null, null)
        with(cursor){
            while (this?.moveToNext()!!){
                val dataEmail = cursor?.getString(cursor.getColumnIndex(MyDBNameClass.COLUMN_EMAIL))
                val dataPassword = cursor?.getString(cursor.getColumnIndex(MyDBNameClass.COLUMN_PASSWORD))
                if(Email == dataEmail && Password == dataPassword) dataList = true
            }
        }
        cursor?.close()
        return dataList
    }

    fun closeDB(){
        myDBHelper.close()
    }
}