package com.example.fragmentsfragmentslifecyclenavigation

interface ActivityInterractor {
    fun OnFragmentClosed()
    fun setupRecyclerAdapter()
}
